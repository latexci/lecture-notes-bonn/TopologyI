set -e
eval $(ssh-agent -s)
echo "$GITLAB_DEPLOY_KEY" | base64 -d | tr -d '\r' | ssh-add - > /dev/null

mkdir -p ~/.ssh
chmod 700 ~/.ssh

ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git config --global user.email "git@maximilian-kessler.de"
git config --global user.name "Maximilian Keßler (via gitlab runner)"
