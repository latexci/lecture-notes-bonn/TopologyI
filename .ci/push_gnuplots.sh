set -e
cd gnuplots
git init
git checkout -b gnuplots
git add .
git commit -m "Create gnuplots"
git remote add origin git@gitlab.com:latexci/lecture-notes-bonn/TopologyI
git push -f --set-upstream origin gnuplots
