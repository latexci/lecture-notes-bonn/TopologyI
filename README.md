# Topology I

These are my lecture notes for the 'Topology I', taught by [Daniel Kasprowski](http://www.math.uni-bonn.de/people/daniel/) in the summer term 2021 at the University of Bonn. There is no guarantee for completeness or correctness.

- The homepage of the course can also be found [here](http://www.math.uni-bonn.de/people/daniel/2021/topo1/)
- You can download the [latest version](https://latexci.gitlab.io/lecture-notes-bonn/TopologyI/2021_Topology_I.pdf) or have a look at the generated [log files](https://latexci.gitlab.io/lecture-notes-bonn/TopologyI/2021_Topology_I.log). These are made available via GitLab CI and a custom runner.

# Contribute
I am glad for corrections or even contributions of any kind to improve these lecture notes. Feel free to send pull request / use the Issues feature here on GitHub or send me corrections / suggestions via Mail.

It would be especially nice if there was someone willing to draw some figures, as I do not have time for these. Just copy-pasting the handdrawn ones of Daniel into the notes would also be nice for a start, so that one does not need to have a look at the uploaded notes in parallel all the time. If you want to do so, write me a mail.
