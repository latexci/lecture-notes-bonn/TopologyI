%! TeX root = ../master.tex

This section has not been part of the lecture.
It was added by the author to clarify some thoughts on the different versions of
Mayer Vietoris sequences (theorems) that were discussed in more detail in the lecture,
especially because of the multiple reformulations of these theorems in the lecture
regarding the generalization to $H_*$-isomorphisms instead of excisive triads.

We have seen \autoref{thm:mayer-vietoris} and \autoref{thm:mayer-vietoris-for-subspace-of-excisive-triad}
as versions of Mayer-Vietoris sequences with slightly different conditions.

We want to present some general remarks/facts about these and put them into context.

\begin{lemma*}
  \label{lm:mayer-vietoris-abstract-nonsense}
  Let $C_\chainbullet $ and $D_\chainbullet $ be long exact sequences,
  and left $f_\chainbullet \colon C_\chainbullet  \to  D_\chainbullet $
  be a chain map such that $f_{3n}$ is an isomorphism for all $n$.

  Then, there is a long exact sequence, the Mayer-Vietoris sequence,
  given by
  \[
  \begin{tikzcd}[column sep = 5em]
    C_{3n+2}
    \ar{r}{\partial _{3n+2} \oplus f_{3n+2}} &  
    C_{3n+1} \oplus D_{3n+2}
    \ar[draw=none]{d}[name=X,anchor=center]{}
    \ar{r}{\partial _{3n+2} - f_{3n+1}} &  
    D_{3n+1}
    \ar[rounded corners,
              to path={ -- ([xshift=2ex]\tikztostart.east)
                        |- (X.center) \tikztonodes
                        -| ([xshift=-2ex]\tikztotarget.west)
                        -- (\tikztotarget)}]{dll}[at end]{\partial } \\
    C_{3n-1}
    \ar{r}{\partial _{3n-1} \oplus f_{3n-1}} &
    C_{3n-2} \oplus D_{3n-1}
    \ar{r}{\partial 3n-1 - f_{3n-2}} &
    D_{3n-2}
  \end{tikzcd}
\]
  where $\partial$ is given as the composition
  \[
  \begin{tikzcd}
    D_{3n+1} \ar{r}{\partial _{3n+1}} &  D_{3n} & C_{3n} \ar{l}{\cong}[swap]{f_{3n}} \ar{r}{\partial _{3n}} &  C_{3n-1}
  \end{tikzcd}
  \]
\end{lemma*}
\begin{proof}
  Diagram chase in the diagram
  \[
  \begin{tikzcd}
    \ldots \ar{r}
    &
    C_{3n+2}
    \ar{dd}[swap]{f_{3n+2} }\ar{rr}{\partial _{3n+2}}
    \ar[blue]{dr}{{\partial} \oplus f}
    &[-2em] &[-2em]
      C_{3n+1} \ar{dd}{f_{3n+1}} \ar{r}{\partial _{3n+1}}
      &
      C_{3n} \ar{dd}[name=X]{f_{3n}}[near end]{\cong} \ar{r}{}
      &
      C_{3n-1}
      \ar{r}
      \ar{dd}{f_{3n-1}}
      &
      \ldots
      \\
      & & D_{3n+2} \oplus C_{3n+1} \ar[blue]{dr}{{\partial} - f}
      \\
    \ldots \ar{r}
    &
    D_{3n+2}\ar[swap]{rr}{\partial _{3n+2}}
      & & 
      D_{3n+1}
      \ar[swap]{r}{\partial _{3n+1}}
      \ar[rounded corners, out = 0, blue,
                to path={ -- ([yshift=1ex,xshift=1ex]\tikztostart.north)
                          -| ([xshift=-3ex]X.center) \tikztonodes
                          |- ([yshift=-1ex,xshift=-1ex]\tikztotarget.south)
                          -- (\tikztotarget)}
         ]{uurr}[at end]{\partial }
      &
      D_{3n} \ar[swap]{r}{}
      &
      D_{3n-1}
      \ar{r}
      &
      \ldots
  \end{tikzcd}
  \]
  The sequence is indicated in blue.
  Note that the leftmost square is not a commutative one here.

  The diagram chase is essentially the same as the one
  given in the lecture to proof \autoref{thm:mayer-vietoris}
  and will thus not be spelled out here.
\end{proof}

\begin{refproof}*{thm:mayer-vietoris}
  Consider the long exact pair sequences of the pairs of spaces
  $(X_1,X_0)$ and $(X,X_2)$.
  The map $f$ induces a chain map between these.
  Since  $f$ is assumed to be an  $H_*$-isomorphism,
  we know that $f_*\colon H_n(X_1,X_0) \xrightarrow{\cong} H_n(X,X_2)$
  is an isomorphism.

  We can thus apply \autoref{lm:mayer-vietoris-abstract-nonsense} to deduce our claim.
\end{refproof}

We also note that
\autoref{thm:mayer-vietoris-for-subspace-of-excisive-triad}
was formulated for excisive triads, which are more restrictive than the
$H_*$ isomorphism case considered in  \autoref{thm:mayer-vietoris}.
We thus give the corresponding formulation here for sake of completeness.

\begin{definition}[Triple map]
  Let $(X_1,X_0,A)$ and $(Y_1,Y_0,B)$ be triples of spaces.
  A \vocab{triple map} is a map $f\colon X_1 \to  Y_1$ such that
  $f(X_0) \subset Y_0$ and $f(A) \subset B$.
\end{definition}

\begin{abuse}
  As usual, we will abuse $f\colon X_1 \to  Y_1$ to refer to the triple map as well.
\end{abuse}

\begin{remark}
  Alternatively, we could state that $f\colon X_1 \to  Y_1$ is a map such that
  $f\colon (X_1,X_0) \to  (Y_1,Y_0)$
  and
  $\frestriction{f}{X_0} \colon (X_0,A) \to  (Y_0,B)$
  are maps of pairs.
\end{remark}

\begin{theorem}[Mayer-Vietoris for triples of spaces]
  \label{thm:mayer-vietoris-triple-of-spaces}
  Let $(X_1,X_0,A)$ and $(X,X_2,B)$ be two triples of spaces and let
  $f\colon X_1 \to  X$ be a triple map.
  Assume further that $f\colon (X_1,X_0) \to  (X,X_2)$ is
  an  $H_*$-isomorphism.

  Then, there is a long exact sequence, the Mayer-Vietoris-sequence,
  given by
  \[
  \begin{tikzcd}[column sep = 5em]
    H_n(X_0,A)
    \ar{r}{i \oplus f_*} &  
    H_n(X_1,A) \oplus H_n(X_2,B)
    \ar[draw=none]{d}[name=X,anchor=center]{}
    \ar{r}{i-f_*} &  
    H_n(X,B)
    \ar[rounded corners,
              to path={ -- ([xshift=2ex]\tikztostart.east)
                        |- (X.center) \tikztonodes
                        -| ([xshift=-2ex]\tikztotarget.west)
                        -- (\tikztotarget)}]{dll}[at end]{\partial } \\
    H_{n-1}(X_0,A)
    \ar{r}{i\oplus f_*} &
    H_{n-1}(X_1,A) \oplus H_n(X_2,B)
    \ar{r}{H_{n-1}(X,B)} &
    D_{3n-2}
  \end{tikzcd}
\]
where the boundary map $\partial $ is given by the composition
\[
  \begin{tikzcd}
    H_n(X,B)  \ar{r}
    &
    H_n(X,X_2)
    &
    H_n(X_1,X_0) \ar{l}{\cong}[swap]{f_*} \ar{r}{\partial}
    &
    H_{n-1}(X_0,A)
  \end{tikzcd}
\]
and the last $\partial $ is the boundary operator of the triple sequence
of $(X_1,X_0,A)$.
\end{theorem}

\begin{proof}
  Consider the triple sequences of $(X_1,X_0,A)$
  and $(X,X_2,B)$ and the induced chain map $f_*$.
  Note that for this, we actually have $f_*$ 
  as the induced maps on homology of the three maps pairs of spaces
  $f \colon (X_0,A) \to  (X_2,B)$, $f\colon (X_1,A) \to  (X,B)$
  and $f\colon (X_1,X_0) \to  (X,X_2)$
  and we need to check that these do indeed form a chain map.

  Now, just note that $f_*\colon H_n(X_1,X_0) \to  H_n(X,X_2)$
  is an isomorphism by assumption and apply
  \autoref{lm:mayer-vietoris-abstract-nonsense}
  to these sequences.
\end{proof}

\begin{remark}
  Note that now
  \autoref{thm:mayer-vietoris-triple-of-spaces}
  is both a generalization of
  \autoref{thm:mayer-vietoris}
  and
  \autoref{thm:mayer-vietoris-for-subspace-of-excisive-triad}.

  We obtain the first version by setting $A = B = \emptyset$,
  and we obtain the second version by constructing the triple map
  \[
    ι\colon  (X_1, X_1 \cap X_2, A) \to  (X,X_2,A)
  \] 
  from the excisive triad $(X,X_1,X_2)$ and the subspace $A\subset X_1\cap X_2$.
  The condition on the $H_*$-isomorphism is then satisfied by the
  definition of an excisive triad.
\end{remark}

\begin{remark}
  In particular, \autoref{thm:mayer-vietoris-triple-of-spaces}
  now gives us the possibility to get triple Mayer-Vietoris sequences
  from pushouts of NDR pairs when we have some subspace of
  the gluing space in the pushout.
\end{remark}
