\begin{minipage}{.8\hsize}\parskip=\smallskipamount
\begin{sheetexercise}
(\textit{$\it3+\it5+\it2$ points}) Let $g\ge1$. The \emph{surface of genus $g$} is the quotient $\Sigma_g$ of the $4g$-gon obtained by identifying pairs of edges as indicated on the right for $g=3$. More formally, we identify for every $0\le i\le g$ the $4i$-th face with the $4i+2$-th face and the $(4i+1)$-th face with the $(4i+3)$-th face in an orientation reversing manner.

Give $\Sigma_g$ a CW-structure with a single $2$-cell and compute the cellular chain complex and the cellular homology with respect to this CW-structure.

\end{sheetexercise}
\end{minipage}
\begin{minipage}{.2\hsize}\vskip-7pt
  \missingfigure{surface}
\end{minipage}

\begin{sheetexercise}
Let $R$ be a ring, not necessarily commutative.
\begin{enumerate}
\item (\textit{4 points}) Let $M$ be a right $R$-module and let $N$ be a left $R$-module. For any abelian group $T$, a map of sets $f\colon M\times N\to T$ is called \emph{$R$-balanced} if it is additive in each variable (i.e.~$f(m+m',n)=f(m,n)+f(m',n)$ and similarly for the second variable) and moreover satisfies $f(mr,n)=f(m,rn)$ for all $m\in M,n\in N,r\in R$.

Construct an abelian group $M\otimes_RN$ together with an $R$-balanced map $M\times N\to M\otimes_RN$ such that every other $R$-balanced map $M\times N\to T$ factors uniquely through a group homomorphism $M\otimes_RN\to T$.

\item (\textit{3 points}) If $N$ is a left $R$-module and $A$ is an abelian group, then we equip the abelian group $\Hom_{\cat{Ab}}(N,A)$ of all \emph{group homomorphisms} with a right $R$-module structure via $(fr)(n)=f(rn)$. Construct functors
\begin{equation*}
\blank\otimes_R N\colon\Mod_R\to\cat{Ab}\qquad\text{and}\qquad
\Hom_{\cat{Ab}}(N,\blank)\colon\cat{Ab}\to\Mod_R
\end{equation*}
between right $R$-modules and abelian groups that are given on objects by the construction from the previous subtask and by $\Hom_{\cat{Ab}}(N,\blank)$, respectively, and show that they are adjoint to each other.
\item (\textit{$\it1+\it2$ points}) Now assume $R$ is commutative. Equip $M\otimes_R N$ with an $R$-module structure such that $M\times N\to M\otimes_R N$ is $R$-bilinear and every other $R$-bilinear map $M\times N\to T$ factors uniquely through an $R$-linear map $M\otimes_RN\to T$.
\end{enumerate}
\end{sheetexercise}

\begin{sheetexercise} (\textit{$\it2+\it8$ points}) Let $C$ be a chain complex of abelian groups. Show that $\mathbb Q\otimes_{\mathbb Z} C$ is again a chain complex and construct natural isomorphisms $H_n(\mathbb Q\otimes_{\mathbb Z} C)\cong\mathbb Q\otimes_{\mathbb Z} H_n(C)$ for all $n\ge0$.
\end{sheetexercise}

\begin{sheetexercise}
\begin{enumerate}
\item (\textit{4 points}) Let $0\to A\to B\to C\to0$ be an exact sequence of finitely generated abelian groups. Show that $\rk(B)=\rk(A)+\rk(C)$, where for a finitely generated abelian group $G$ we write $\rk(G)$ for its \emph{rank}, i.e.~the unique integer $r$ such that $G\cong\mathbb Z^r\oplus T$ for some finite group $T$.

\item (\textit{$\it5+\it1$ points}) Let $C$ be a bounded chain complex (i.e.~$C_n=0$ for almost all $n\in\mathbb N$) of finitely generated abelian groups. Show that $\sum_{n\ge0}(-1)^n\rk C_n=\sum_{n\ge0}(-1)^n\rk H_n(C)$.

Conclude that the Euler characteristic of a finite CW-complex is a homotopy invariant, i.e.~if $X,Y$ are homotopy equivalent finite CW-complexes, then $\chi(X)=\chi(Y)$.
\end{enumerate}
\end{sheetexercise}

The goal of the following bonus exercise is to provide the missing ingredient to the proof that any homology theory that satisfies the Dimension Axiom and the Disjoint Union Axiom agrees with singular homology on all CW-complexes. Since this will require a bit more work than usual, this task is worth 20 bonus points and only due \textbf{December 20} (i.e.~when you will have to hand in the next sheet).

\begin{sheetexercise*}
Throughout let $\mathcal H_*$ be \emph{any} homology theory.
\begin{enumerate}
\begin{minipage}{.81\hsize}
\item (\textit{6 bonus points}) Let $n\ge1$, let $X$ be a space, and let $f\colon I^n\to X$ be any map, where $I=[0,1]$ denotes the unit interval. For $k\in\mathbb N$ and $(i_1,\dots,i_n)\in \{0,\dots,k-1\}^n$ we define the map $f_{i_1,\dots,i_n}\colon I^n\to X$ via $f_{i_1,\dots,i_n}(t_1,\dots,t_n)=f\big((i_1+t_1)/k,\dots,(i_n+t_n)/k)$. Put differently, we have subdivided the cube as indicated schematically on the right for $n=2,k=4$, and $f_{i_1,\dots,i_n}$ is the restriction to one of the subcubes (rescaled suitably).
\end{minipage}\hskip-2.6em
\begin{minipage}{.19\hsize}
  \missingfigure{subdivision}
\end{minipage}


Now let $A\subset X$ be arbitrary, and assume that each $f_{i_1,\dots,i_n}$ is a map of pairs $(I^n,\del I^n)\to (X,A)$; in particular, $f$ itself is such a map of pairs. Show that
\begin{equation*}
f_*=\sum_{0\le i_1,\dots,i_n\le k} (f_{i_1,\dots,i_n})_*
\end{equation*}
as maps ${\mathcal H}_*(I^n,\del I^n)\to{\mathcal H}_*(X,A)$.

\textbf{Hint.} Let $K\subset I^n$ be the union of the boundaries of the little cubes of the subdivision. As a first step, show that the inclusions of the small subcubes induce an isomorphism $\bigoplus_{i_1,\dots,i_n}\mathcal H_*(I^n,\del I^n)\to\mathcal H_*(I^n,K)$ and construct a (left) inverse to this in terms of suitable `collapse maps.'
\end{enumerate}

Using this, we now want to prove for all $n\ge1$:

\begin{theorem}
If $f\colon S^n\to S^n$ has degree $d$, then $f_*\colon\widetilde{\mathcal H}_k(S^n)\to\widetilde{\mathcal H}_k(S^n)$ is multiplication by $d$ for every $k\in\mathbb Z$.
\end{theorem}

For this, proceed as follows:

\begin{enumerate}[resume]
\item (\textit{$\it1+\it2$ bonus points}) Let $s\in S^n$ arbitrary. Show that $f$ can be homotoped to a map $f'$ with $f'(s)=s$, and conclude that it suffices to show that $f'_*\colon\mathcal H_k(S^n,s)\to\mathcal H_k(S^n,s)$ is multiplication by $d$ for all $k\in\mathbb Z$. Use this to prove the above theorem for $n=1$.

\textbf{Hint.} You know all self maps of the pair $(S^1,s)$ up to homotopy.

\item (\textit{2 bonus points}) Assume we know the theorem for $S^{n-1}$, and let $f\colon (D^n,S^{n-1})\to (D^n,S^{n-1})$ be any map inducing multiplication by $d\in\mathbb Z$ on $H_n^{\text{sing}}(D^n,S^{n-1})$. Show that $f_*\colon\mathcal H_k(D^n,S^{n-1})\to\mathcal H_k(D^n,S^{n-1})$ is multiplication by $d$ for all $k\in\mathbb Z$.

\item (\textit{8 bonus points}) Now let $s\in S^n$ be a point on the equator, and let $f\colon S^n\to S^n$ be any map of degree $d$ with $f(s)=s$. Still assuming we know the theorem for $S^{n-1}$, show that $f_*\colon\mathcal H_k(S^n,s)\to\mathcal H_k(S^n,s)$ is multiplication by $d$ for all $k\in\mathbb Z$.

\textbf{Hints.} Write $U=S^n\setminus\{(1,0,\dots0)\}$ and reduce the above claim to computing the effect of maps $g\colon(I^n,\del I^n)\to (S^n,U)$ on homology. Then use cellular approximation together with the Lebesgue Lemma to show that after a suitably fine subdivision we can pair homotope any such $g$ to a map $g'$ for which each $g'_{i_1,\dots,i_n}$ factors through one of the two hemisphere inclusions $(D^n,S^{n-1})\to (S^n,S^{n-1})$.

\item (\textit{1 bonus point}) Deduce the theorem.
\end{enumerate}
\end{sheetexercise*}
