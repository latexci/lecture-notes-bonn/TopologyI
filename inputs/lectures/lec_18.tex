%! TEX root = ../../master.tex
\lecture[]{Do 16 Dez 2021}{Untitled}

\begin{orga}
    \begin{itemize}
        \item The due date of the bonus exercise has been changed to the \nth{10} of January
            (instead of Dec \nth{20})
        \item On eCampus you can find a poll regarding the future topics of the Q\&A session.
        \item Recall that on Dec \nth{20}, there will be a lecture,
            and the  Q\&A session will instead take place (online) on the \nth{22}.
    \end{itemize}
\end{orga}

\begin{editor}
    \autoref{lm:exactness-of-hom-for-free-modules} has been corrected
    as mentioned in the beginning of the \nth{18} lecture.
\end{editor}

\begin{corollary}\label{cor:ses-of-r-cochain-complexes-for-cohomlogy-with-coefficients}
    Let $(X,A)$ be a pair of spaces and let  $M$ be a left  $R$-module.
    Then there is a short exact sequence of  $R$-cochain complexes
    \[
    0
    \to  C^{\chainbullet}(X,A;M)
    \to C^{\chainbullet}(X;M)
    \to C^{\chainbullet}(A;M)
    \]
    As for chain complexes, we get an induced map
    \[
        δ\colon H^n(A;M) \to H^{n+1}(X,A;M)
    \] 
\end{corollary}
\begin{proof}*
    Apply $\Hom_R(-,M)$ to the short exact sequence of singular homology:
     \[
         0
         \to  C_\chainbullet (A;R)
         \to C_\chainbullet (X;R)
         \to C_\chainbullet (X,A;R)
         \to 0
    \]
    The resulting sequence will be exact by
    \autoref{lm:exactness-of-hom-for-free-modules}.
    Then this yields the desired long exact sequence using
    \autoref{thm:exact-sequence-of-chain-complexes-induces-les-on-homology}
    as in the case for homology.
\end{proof}

\begin{theorem}\label{thm:singular-cohomlogy-is-cohomology-theory-with-extra-axioms}
    $H^{\chainbullet}(X,A;M)$ is a cohomology theory
    that satisfies the disjoint union axiom and
    the modified dimension axiom.
\end{theorem}
\begin{proof}[Sketch of proof]
    If $h\colon C_* \to D_{* - 1}$ is a chain homotopy between
    $f_*, g_*$, then 
    \[
        h^*\colon \Hom(D_*, M) \to  \Hom(C_{*-1}, M)
    \] 
    is a (co)chain homotopy between $g^*$,  $f^*$. (exercise).
    Hence  $H^*(-;M)$ is homotopy invariant,
    as singular homology is.

    As before, one shows that
     \[
         C^*(X,B;M) \to C^*(X\setminus A, B\setminus A;M)
    \] 
    is an isomorphism on $H^*$.
    Actually,
     \[
         C^{\sing}_{*}(X\setminus A, B\setminus A) \to  C_*^{\sing}(X,B)
    \] 
    is a chain homotopy equivalence.

    The LES of pairs property holds since we used the same construction for the
    connecting homomorphism as in the homology case.

    Now consider $X = \coprod _{i \in I}X_i$, then the chain isomorphism
    \[
        C_*^{\sing}(X;R) \stackrel{\cong}{\leftarrow}
        \bigoplus _{i \in I}C_*^{\sing}(X_i;R)
    \] 
    induces an isomorphism
    \begin{IEEEeqnarray*}{rCl}
        C^*(X;M) & \stackrel{\cong}{\longrightarrow} &
        \Hom_R(\bigoplus_{i \in I} C_*^{\sing}(X_i;R), M) \\
                 & \cong & \prod _{i \in I}\Hom_R(C_*^{\sing}(X;R),M) \\
                 &\cong& \prod_{i \in I}C^*(X_i;M)
    \end{IEEEeqnarray*}
    and thus also
    \[
        H^*(X;M) \cong \prod_{i \in I}H^*(X_i;M)
    \] 
    with the maps $H^*(X;M) -gH^*(X_i;M)$ induced by the inclusions  $X_i \hookrightarrow X$.

    For the dimension axiom, note that
    \[
        C^*(\text{pt}, M) = 
        \ldots \leftarrow M \stackrel{\id}{\leftarrow} M
        \stackrel{0}{\leftarrow} M
        \stackrel{\id}{\leftarrow} M
        \stackrel{0}{\leftarrow} M
        \leftarrow 0
    \] 
    and thus we get
    \[
        H^*(\text{pt};M) \cong \begin{cases}
            M & \star = 0 \\
            0 & \text{else}
        \end{cases}
    \] 
\end{proof}

\begin{dremark}
    The fact that $C_*^{\sing}(X\setminus A, B\setminus A) \to  C_*^{\sing}(X,B)$ 
    is a chain homotopy equivalence can also be shown more abstractly by the following:

    We already know that this is a quasi-isomorphism (i.e. a map that induces isomorphism on $H_*$)
    between free  $\mathbb{Z}$-chain complexes.

    Since $\mathbb{Z}$ is a principal ideal domain, the result follows.
\end{dremark}

\begin{recap}
    A quick revision of direct sums, products, and their differences:

    A map $\bigoplus M_i \to  N$ is - by definition of the coproduct -
    equivalently given by an arbitrary family of maps $M_i \to N$ for each $i$.

    Note that an element of  $\bigoplus M_i$ is a sequence $(m_i)_i$ with  $m_i \in M_i$
    and $m_i = 0$ for almost all  $i\in I$.

    So for such an element and a family of maps $f_i$,
    for the induced map $f = \oplus_{i \in I} f_i$ we get that
     \[
         f((m_i)_{i \in I}) = \sum_{i \in I}f_i(m_i)
    \] 
    is well-defined, since the sum consists of finitely many non-zero summands.

    Finally note that a map $N \to \prod_{i \in I} M_i$ is,
    again by the definition of products in arbitrary categories,
    given by a family of maps $f_i \colon N \to  M_i$ for each $i\in I$.
    In contrast, this is not the case for the direct sum,
    since maps $f_i\colon N \to  M_i$ do not necessarily have image 0
    for all but finitely many of them for each $n\in N$.
    This is only the case for $I$ finite.
\end{recap}

\begin{lemma}\label{lm:natural-pairing-morphism-for-cohomlogy-and-homology-of-chain-complex}
  Let $C_{\chainbullet}$ be an  $R$-chain complex and let $M$ be a left $R$-module. 
   Then there is a natural map
       \begin{equation*}
       \left< -, - \right> : \left| \begin{array}{c c l} 
           H^n(\Hom_R(C_\chainbullet ,M)) & \longrightarrow & \Hom_R(H_n(C_\chainbullet ),M) \\
       \left[ \varphi \right]  & \longmapsto &  \left< \left[ \varphi \right] ,- \right> 
       \end{array} \right.
   \end{equation*}
   
\end{lemma}
\begin{proof}
    Pick some $[\varphi] \in H^n(\Hom_R(C_*, M))$.
    Then this is represented by some $\varphi\colon C_n \to M$ such that
    \[
        C_{n+1} \stackrel{d_{n+1}}{\longrightarrow} C_n \stackrel{\varphi}{\longrightarrow} M
    \] 
    is trivial.
    By the universal property of the cokernel,
    this thus induces a map
    \[
    \overline{\varphi}\colon \coker d_{n+1} \to M
    \] 
    and we can restrict it to a map
    \[
        \frestriction{\overline{\varphi}}{H_n(C_*)} \colon H_n(C_*) \to M
    \]
    since
    \[
        H_n(C_*) = \faktor{\ker d_n}{\im d_{n+1}} \subset \faktor{C_n}{\im d_{n+1}}
        \cong \coker d_{n+1}
    \] 
    It remains to show that  $\frestriction{\overline{\varphi}}{H_n(C_*)}$ 
    depends only on the cohomology class of $\varphi$.
    For this, it suffices to show that $\frestriction{\overline{\varphi}}{H_n(C_*)}$
    is trivial if $\varphi \in \im \Hom_R(d_n)$, i.e. if
    $\varphi$ is of the form
    \[
      \varphi\colon   C_n \stackrel{d_n}{\longrightarrow} C_{n-1}
        \stackrel{\psi }{\longrightarrow} M
    \]
    But this is true since we restricted $\overline{\varphi}$ to the kernel of $d_n$.
\end{proof}

\begin{fact}\label{fact:submodule-of-free-r-module-is-free-if-r-is-a-pid}
    Let $R$ be a principal ideal domain.
    Then every submodule of a free  $R$-modules is free.
\end{fact}

\begin{placeholder}
    Since the facts are sub-numbered in these notes currently, this takes the place of
    \autoref{fact:submodule-of-free-r-module-is-free-if-r-is-a-pid}
    until this is fixed to ensure correct numbering of proceeding environments.
\end{placeholder}

\begin{lemma}\label{lm:curried-kronecker-pairing-is-split-surjective}
    Let $R$ be a principal ideal domain and  $C_*$
    a free  $R$-chain complex
    (i.e. all $C_n$ are free  $R$-modules).
    Then the map $\left< -, - \right> $ from
    \autoref{lm:natural-pairing-morphism-for-cohomlogy-and-homology-of-chain-complex}
    is split surjective.
\end{lemma}
\begin{proof}
    The sequence
    \[
    0 \to  \ker d_n \to  C_n \to  \im d_n \to  0
    \] 
    is exact.
    By \autoref{fact:submodule-of-free-r-module-is-free-if-r-is-a-pid},
    $\im d_n \subset C_{n-1}$ is free, so the sequence splits.

    So we may pick a projection $C_n \stackrel{p}{\longrightarrow} \ker d_n$.

    For a map $f\colon \faktor{\ker d_n}{\im d_{n+1}} \to  M$,
    we can thus consider
    \[
    \varphi_f \colon C_n \stackrel{p}{\longrightarrow} \ker d_n
    \twoheadrightarrow \faktor{\ker d_n}{\im d_{n+1}}
    \stackrel{f}{\longrightarrow} M
    \]
    \begin{claim}
        $\varphi_f \in \ker d_{n+1}^*$,
        and $[\varphi_f] \in H^n(\Hom_R(C_*,M))$
        is a preimage of $\varphi$ under $\left< -,- \right> $.
    \end{claim}
    \begin{proof}
       First note that for the composition $\varphi_f \circ d_{n+1}$, we have
       \[
       \begin{tikzcd}
           C_{n+1} \ar{r}{d_{n+1}} \ar{dr}[swap]{d_{n+1}} \ar[bend left = 20]{rrr}{0}& 
           C_n \ar{r}{p} &  \ker d_n \ar[two heads]{r} & 
           \faktor{\ker d_n}{\im d_{n+1}} \ar{r}{f} &  M \\
                                                    & \ker d_n \ar{u} \ar[swap]{ur}{\id}
       \end{tikzcd}
       \]
       so that indeed $\varphi_f \in \ker d_{n+1}^*$,
       and $[\varphi_f]$ is an element of the homology.

       Then by construction, $\overline{\varphi_f}$ is given by
       \[
           \faktor{C_n}{\im d_{n+1}}
           \stackrel{\overline{p}}{\longrightarrow} \faktor{\ker d_n}{\im d_{n+1}}
           \stackrel{f}{\longrightarrow} M
       \] 
       and restricting to $\faktor{\ker d_n}{\im d_{n+1}}$ yields back $f$,
       since  $p$ was a splitting, so that  $p \circ  ι = \id_{\ker d_n}$.

       Thus $f \mapsto \varphi_f$ is a splitting.
    \end{proof}
\end{proof}

\begin{remark**}\label{rm:split-surjective-maps}
    We say that a map $f\colon A \to B$ is split surjective,
    if there is a splitting $g\colon B \to  A$ such that $f \circ  g = \id_B$.

    Note that this immediately gives that $f$ is surjective
    (therefore the naming), but is in general a stronger statement.
\end{remark**}

\begin{definition}\label{def:kronecker-pairing}
    The \vocab{Kronecker pairing} is the map
        \begin{equation*}
        \left< -, - \right> : \left| \begin{array}{c c l} 
            H^n(X, A;M) \otimes H_n(X, A; R) & \longrightarrow & M \\
        \varphi \otimes x & \longmapsto &  \left< \varphi,x \right> 
        \end{array} \right.
    \end{equation*}
\end{definition}

\begin{lemma}\label{lm:pullback-pushforward-of-kronecker-pairing}
    For a map of pairs of spaces $f\colon (X,A) \to (Y,B)$,
    a cohomology class
    $\left[ \varphi \right] \in H^n(Y,B;M)$,
    and a homology class $[x] \in H_n(X,A;R)$, we have
    \[
    \left< f^*\left[ \varphi \right] ,\left[ x \right]  \right> 
    = \left< \left[ \varphi \right] , f_*\left[ x \right]  \right> 
    \] 
    and for $\left[ \psi  \right] \in H^{n-1}(A;M)$ we have
    \[
    \left< δ\left[ \psi  \right] , \left[ x \right]  \right> 
    = \left< \left[ \psi  \right] , \partial \left[ x \right]  \right> 
    \] 
\end{lemma}

\begin{proof}
    $\left< f^* \left[ \varphi \right] , \left[ x \right]  \right> $
    describes the evaluation of the map $f^*\varphi$ at $x\in \ker d_n$.
    By commutativity of
    \[
    \begin{tikzcd}
        x \ar[phantom]{r}{\in } \ar{d}{f_*}& \ker d_n \ar[hook]{r}& 
        C_n(X,A) \ar{d}{f_*}  \ar[dashed]{dr}{f^* \varphi}\\
        f_*x \ar[phantom]{r}{\in} & \ker d_n \ar[hook]{r} & C_n(Y,B) \ar{r}{\varphi} & M
    \end{tikzcd}
    \]
    we see that this is the same as the evaluation of $\varphi$ at $f_*x$.

    What is $δ\left[ \psi  \right] \in H^n(X,A;M)$?

    Extend $\psi \colon C_{n-1}(A;R) \to M$ in some way to
    $\tilde{\psi}\colon C_{n-1}(X;R) \to M$.
    Then $\tilde{\psi } \circ d_n\colon C_n(X;R) \to  M$ is trivial
    when restricted to $C_n(A;R)$ and thus defines
    \[
        \overline{\psi }\colon C_n(X,A;R) \to M
    \]
    Then $δ\left[ \psi  \right]  = \left[ \overline{\psi } \right] $.
    So
    \[
        \left< δ\left[ \psi   \right] , \left[ x \right]  \right> = \overline{\psi }(x)
    \] 
    By commutativity of
    \[
    \begin{tikzcd}
        &C_n(X;R) \ar[swap]{d}{d_n} \ar{r}{} & C_n(X,A;R)  \ar{r}{\overline{\psi }} & M\\
        C_{n-1}(A;R) \ar{r}    \ar[bend right = 40]{rrru}{\psi }
        &   C_{n-1}(X;R) \ar{urr}[swap]{\tilde{\psi }} & 
    \end{tikzcd}
    \]
    we deduce
    \[
        \overline{\psi }(x)
        = \tilde{\psi } (d_n(\overline{x}))
        = \psi (d_n(\overline{x})) \in C_{n-1}(A;R)
    \]
    but $\left[ d_n(\overline{x}) \right]  = \partial \left[ x \right] $ and thus our claim
    \[
    \left< δ\left[ \psi  \right] , \left[ x \right]  \right> 
    = \left< \left[ \psi  \right] , \partial \left[ x \right]  \right> 
    \] 
    follows.
\end{proof}

\begin{goal}
    We now want to analyze the kernel of $\left< -, - \right> $ even further.
    For this, we need more homological algebra.
\end{goal}

\begin{definition}\label{def:free-resolution}
    Let $M$ be a (left) $R$-module. 
    A \vocab{free resolution} of $M$ is an exact sequence
     \[
    \ldots \to  F_n \to F_{n-1} \to  \ldots \to F_1 \to  F_0 \to  M \to 0
    \]
    with $F_i$ a free (left)  $R$-module.
\end{definition}

\begin{dlemma}
    Let $R$ be a PID and let  $M$ be an  $R$-module.
    Then there is a resolution of $M$ with  $F_j = 0$ for  $i\geq 2$.

    If $R$ is a PID, the kernel of  $F_0 \to M$ is free and thus
    there always is a free resolution
\end{dlemma}
\begin{proof}
    We can first find a surjection $F_0 \twoheadrightarrow M$ with $F_0$ free.
    Then by \autoref{fact:submodule-of-free-r-module-is-free-if-r-is-a-pid},
    the kernel of $F_0 \to M$ is given by a free submodule $F_1\subset F_0$.
    Then
    \[
    0 \to  0 \to \ldots \to  0 \to  F_1 \to  F_0 \to M \to  0
    \]
    is exact.
\end{proof}

\begin{dlemma}
    For each (left) $R$-module $M$, there exists a free resolution of $M$.
\end{dlemma}
\begin{proof}
    Pick $F_0 = R[M]$, and the surjective morphism
        \begin{equation*}
        \begin{array}{c c l} 
        F_0 & \longrightarrow & M \\
        \sum r_i m_i & \longmapsto &  r_i m_i
        \end{array}
    \end{equation*}
    and then inductively set
    \[
        F_i = R\left[ \ker (F_{i-1} \to F_{i-2}) \right] 
    \] 
    with the corresponding projection.
\end{proof}

\begin{definition}\label{def:ext-groups}
    Let $M,M'$ be left  $R$-modules.
    We define $\Ext_R^n(M,M')$ as follows:

    Let  $F_*$ be a free resolution of $M$.
    Then we get a cochain complex
    \[
        C^* = \Hom_R(F_*, M')
    \] 
    and we define
    \[
        \Ext^n_R(M,M') = H^n(C^*)
    \] 
\end{definition}

\begin{lemma}\label{lm:induced-chain-map-of-free-resolutions}
    Let $f\colon M\to M'$ be an $R$-module homomorphism
    and let $F_*$,  $F'_*$ be two free resolutions.

    Then there exists a chain map $f_*\colon F_* \to  F_*'$ inducing $f$ on $H_0$.

    Moreover any two such  $f_*$ are chain homotopic.
\end{lemma}

\begin{corollary}\label{cor:ext-groups-are-well-defined-and-functorial}
    $\Ext_R^n(M,M')$ is well-defined and functorial in  $M$ (and $M'$).
\end{corollary}
\begin{proof}
    We want to show that $\Ext_R^n(M,M')$ does not depend on the free resolution of $F_*$.
    So let $F'_*$ be another free resolution.

    Then by applying \autoref{lm:induced-chain-map-of-free-resolutions}
    there exist chain maps $f_*\colon F_* \to  F_*'$
    and $g_*\colon F_*' \to F_*$ 
    inducing $\id_M$ on  $H_0$.

    Then also $f_* \circ g_*\colon F_*' \to  F_*'$
    and $g_* \circ f_*\colon F_* \to F_*$ 
    induce $\id_M$ on  $H_0$.
    By  \autoref{lm:induced-chain-map-of-free-resolutions}
    these are chain homotopic to $\id_{F_*'}$ and $\id_{F_*}$.
    Thus, $f_*$ is a chain homotopy equivalence with inverse  $g_*$.
    So $f^*\colon \Hom_R(F_*', M') \to \Hom_R(F_*,M)$ is a cochain homotopy equivalence,
    and $f^*$ induces an isomorphism on $H^*$.
    Again by \autoref{lm:induced-chain-map-of-free-resolutions}
    this isomorphism is independent on the choice of $f_*$.
    Hence $H^*(\Hom_R(F_*,M'))$ and  $H^*(\Hom_R(F_*',M'))$
    are canonically isomorphic, and $\Ext_R^n(M,M')$ is well-defined.

    Functoriality follows similarly.
\end{proof}
