%! TEX root = ../../master.tex
\lecture[]{So 07 Nov 2021}{Untitled}

\begin{proof}
Note that $j \circ  i$ factors as
\[
\begin{tikzcd}
    (B,A)\ar{dr} \ar{r}{i} &  (X,A) \ar{r}{j} &  (X,B) \\
                    & (B,B) \ar{ur}
\end{tikzcd}
\]
but now applying $H_n$ and using  \autoref{lm:homology-of-b-b-is-trivial}, we see that $j_* \circ  i_* = 0$. Now consider the following exact diagram, where the red maps describe the triple sequence whose exactness we want to show.
\[
\begin{tikzcd}[column sep = tiny]
    \bullet && H_{n+1}(X,B) && H_n(B,A) && H_{n-1}(A)  && H_{n-1}(X)  && .  \\
            & \bullet && H_n(B) && H_n(X,A) && H_{n-1}(B) && \bullet \\
     \bullet && H_n(A) && H_n(X) && H_{n}(X,B) && H_{n-1}(B,A) && \bullet
	\arrow[from=1-3, to=2-4, "\partial"]
	\arrow[from=2-4, to=3-5]
	\arrow[from=3-5, to=2-6]
	\arrow[from=2-6, to=1-7, "\partial"]
	\arrow[bend left=40, from=1-7, to=1-9]
	\arrow[from=3-3, to=2-4]
	\arrow[from=2-4, to=1-5]
    \arrow[bend left=40, from=1-5, to=1-7,"\partial"]
	\arrow[from=3-7, to=2-8, "\partial"]
	\arrow[from=2-8, to=1-9]
	\arrow[from=2-8, to=3-9]
	\arrow[bend right=40, from=3-5, to=3-7]
	\arrow[bend right=40, from=3-3, to=3-5]
	\arrow[bend right=40, from=3-1, to=3-3]
	\arrow[bend left=40, from=1-9, to=1-11]
	\arrow[from=1-7, to=2-8]
	\arrow[from=2-2, to=1-3, red]
	\arrow[bend left=40, from=1-1, to=1-3]
	\arrow[bend right=40, from=3-9, to=3-11]
	\arrow[from=1-9, to=2-10]
    \arrow[from=1-3, to=1-5, bend left = 40, red, "\partial"]
    \arrow[from=3-7, to=3-9, bend right = 40, red, "\partial"]
    \arrow[from=1-5, to=2-6, red]
    \arrow[from=2-6, to=3-7, red]
    \arrow[from=3-9, to=2-10, red]
\end{tikzcd}
\]
Note that commutativity of the red maps with the black ones follows from naturality of the boundary operators.

We start with proving that double compositions are zero. We see that
\[
    H_n(X,A) \to  H_n(X,B) \stackrel{\partial}{\longrightarrow} H_{n-1}(B,A) = H_n(X,A)\stackrel{p}{\longrightarrow} H_{n-1}(A) \stackrel{}{\longrightarrow} H_{n-1}(B) \to H_{n-1}(B,A) = 0
\] 
and similarly, the composition at $H_n(B)$ is zero as well.
\begin{editor}
    I will not write up the whole diagram chase, this would just take an extraordinary amount of time, and not provide real benefits, since these kind of diagrams get really messy. You really have to do them yourself, or watch the excellent lecture video for this.
\end{editor}

For the naturality, note that for two such sequences and a corresponding map, one only has to check that 
\[
\begin{tikzcd}
    H_n(X,B) \ar[swap]{d}{} \ar{r}{\partial} & H_{n-1}(B,A) \ar{d}{} \\
    H_n(X',B') \ar[swap]{r}{\partial} & H_{n-1}(B',A')
\end{tikzcd}
\]
commutes again, as the rest follows from functoriality of the homology. But $\partial$ was defined as the following composition:
\[
\begin{tikzcd}
    H_n(X,B) \ar{r}{\partial} \ar{d}&  H_n(B) \ar{r}{} \ar{d}&  H_{n-1}(B,A)\ar{d} \\
    H_n(X',B') \ar{r}{\partial} &  H_{n-1}(B') \ar{r}{} &  H_{n-1}(B',A')
\end{tikzcd}
\]
but now we simply use The naturality of the boundary map for the left square, whereas on the right, we again use functoriality of $H_n$. So indeed, the constructed long exact sequences are natural.
\end{proof}

\begin{theorem}[Mayer-Vietoris]\label{thm:mayer-vietoris-for-subspace-of-excisive-triad}
    Let $(X,X_1,X_2)$ be an excisive triad, denote $X_0 \coloneqq X_1 \cap X_2$. Let $A\subset X_0$ be a subspace. Then there is a natural long exact sequence
    \[
    \begin{tikzcd}
        \ar{r}{\partial} &  H_n(X_0,A) \ar{r}{i_1 \oplus i_2} &  H_n(X_1,A) \oplus H_n(X_2,A) \ar{r}{j_1-j_2} &  H_n(X,A) \ar{r}{\partial} &  H_{n-1}(X_0,A)
    \end{tikzcd}
\]
where the boundary map $\partial $ is given by
\[
\begin{tikzcd}
    \partial\colon H_n(X,A) \stackrel{k_*}{\longrightarrow} (X,X_2) & \ar{l}[swap]{e_x^{-1}}{\cong} H_n(X_1,X_0) \stackrel{\partial}{\longrightarrow} H_{n-1}(X_0,A)
\end{tikzcd}
\]
where the last $\partial$ is the boundary morphism of the triple sequence $(X_1,X_0,A)$.
\end{theorem}
\begin{proof}
    Same as \autoref{thm:mayer-vietoris}, by replacing all $H_n(X_i)$ with  $H_n(X_i, A)$ and we use the triple sequence instead of the pair sequence.
\end{proof}

\begin{definition}\label{def:cone-suspension}
    Let $X$ be a space. Then the  \vocab{cone} $C(X)$ of  $X$ is the space
     \[
         \faktor{X\times [0,1]}{X\times \left \{1\right\} }
    \] 
    The \vocab{suspension} $\sum X$ of  $X$ is the space
     \[
         \faktor{X\times [-1,1]}[-0.3]{\substack{(x,i)\sim (y,i) \\ \forall x,y\in X, i\in \left \{-1,1\right\} }}
    \] 
\end{definition}

\begin{remark}
    \begin{enumerate}[1.]
        \item We have natural inclusions
            \[
                X \to C(X), X \to \sum X
            \] 
            given by $x\mapsto (x,0)$.
        \item $C$ and  $\sum$ are functors  $\Top \to  \Top$.
    \end{enumerate}
\end{remark}

\begin{theorem}\label{thm:suspension-shifts-homology-by-1}
    Let $x\in X$. There is a natural isomorphism (natural in $\Top_{\star}$ )
    \[
        H_n(X,\left \{x\right\} ) \stackrel{\cong}{\longrightarrow} H_{n+1}\left(\sum X, (x,0)\right)
    \] 
\end{theorem}
\begin{proof}
    Note that we have a pushout (natural in $X$)
    \[
    \begin{tikzcd}
        X \ar[swap]{d}{i_1} \ar{r}{i_0} & C(X) \ar{d}{j_0} \\
        C(X) \ar[swap]{r}{\id \times -} & \sum (X)
    \end{tikzcd}
    \]
    As in the proof that $(D^n, S^{n-1})$ is an NDR pair, $(CX, X)$ is also an NDR pair and thus \autoref{prop:pushout-for-ndr-pair-gives-excisive-triad} yields an excisive triad.
    Now by  \nameref{oral:mayer-vietories-for-s-n} applied to $(\sum X, C(X), C(X), \left \{x\right\} )$, we get a sequence
    \[
        \ldots\to H_n(X,x) \to H_n(C(X),(x,0)) \oplus H_n(C(X), (x,0)) \stackrel{j_0-j_1}{\longrightarrow} H_n(\sum X, (x,0)) \stackrel{\partial}{\longrightarrow} H_{n-1}(X,x) \stackrel{}{\longrightarrow} 
    \] 
    As in last term we proved that $C(X) \simeq \star$ is contractible, we deduce $H_n(C(X), (x,0)) = 0$. Thus we deduce by the long exact sequence, that
     \[
         H_{n+1}\left(\sum X, (x,0)\right) \stackrel{\cong}{\longrightarrow}  H_n(X,x)
    \] 
    via $\partial$.
\end{proof}

\section{Calculations and applications}

\begin{proposition}\label{prop:cross-product-with-sphere-shifts-homology-by-dimension}
    Let $X$ be a space,  $d\in \mathbb{N}_0$. There is a natural isomorphism
    \[
        H_n(S^d \times X) \stackrel{\cong}{\longrightarrow} H_n(X) \oplus H_{n-d}(X)
    \] 
    where the first map is induced by the projection.
\end{proposition}
\begin{proof}
    By induction on $d$, we will first prove that for  $s\in S^d$
    \[
        H_n(S^d \times X, \left \{s\right\} \times X) \cong H_{n-d}(X) \tag{1}
    \] 
    that is natural in $X$.

    For $d=0$ we have to show that
     \[
         H_n(S^0\times X, \left \{1\right\} \times X) \cong H_n(\left \{-1\right\} \times X) \cong H_n(X)
    \]
    Note that for applying excision in the first isomorphism, it is really important that $\left \{1\right\} \times X$ is clopen in $S^0 \times X$.

    For the induction step, we have a pushout
    \[
    \begin{tikzcd}
        S^{d-1}\times X \ar[swap]{d}{} \ar{r}{} \pushout& D^d \times X \ar{d}{} \\
        D^d \times X \ar[swap]{r}{} & S^d \times X
    \end{tikzcd}
    \]
    with $(D^d \times X, S^{d-1}\times X)$ an NDR-pair.
    \begin{oral}
        Note that above pushout does not simply by claiming that $S^d$ is a pushout of  $D^d, D^d$ over  $S^{d-1}$ and then crossing with $X$, as crossing with a space does not necessarily preserve pushouts (but e.g. For  $I$ it does, as we showed in  \autoref{lm:pushout-ist-stable-under-product-with-unit-interval}). The argument really is that on set-level, the induced map $S^d\times X\to Y$ (for some $Y$) is clear,
    \end{oral} 
    For $s\in S^{d-1}$ we have
    \[
        H_n(D^d \times X, \left \{s\right\} \times X) = 0
    \] 
    since $\left \{s\right\} \hookrightarrow  D^d$ is a homotopy equivalence.
    Now by \nameref{thm:mayer-vietoris-for-subspace-of-excisive-triad}, we again deduce that
    \[
        H_n(S^d \times X, \left \{s\right\} \times X) \cong H_{n-1}(S^{d-1}\times X, \left \{s\right\} \times X) \stackrel{\text{induction}}{\cong} H_{n-d}(X) 
    \] 
    So the induction is complete.
    Now consider the pair sequence
    \[
    \begin{tikzcd}
        \ar{r}{\partial} & H_n(\left \{s\right\} \times X) \ar{r}{} &  \ar[bend left = 20, red, dashed, "p"]{l}  H_n(S^d\times X)\ar{r} & H_n(S^d\times X, \left \{s\right\} \times X) \ar{r}{\partial} &  \ldots
    \end{tikzcd}
\]
But as this sequence splits, we deduce that
\[
    H_n(S^d\times X) \cong H_n(X) \oplus H_n(S^d \times X, \left \{s\right\} \times X)
\] 
natural in $X$ with the first map induced by the projection. Thus, by our inductive claim, we get
 \[
     H_n(S^d \times X) \cong H_n(X) \oplus H_{n-d}(X)
\] 
\end{proof}

\begin{corollary}[Homology of the torus]
    Denote the $d$-dimensional torus as  $T^d \coloneqq  \prod_{i=1}^d S^1$. Then
    \[
        H_n(T^d) \cong \bigoplus_{i=0}^d \left( \bigoplus_{j=1}^{\binom{d}{i}} H_{n-i}(\star)\right) 
    \] 
    If $H_n$ satisfies the dimension axiom, then
     \[
         H_n(T^d) \cong \begin{cases}
             \mathbb{Z}^{\binom{d}{n}} & 0 \leq  n \leq  d \\
             0
         \end{cases}
    \] 
\end{corollary}
\begin{proof}
    By induction on $d$. For $d=0$,  $T^0 = \star$, and since  $\binom{0}{0} = 1$, the formula is a tautology.

    For the induction step, we see that  $T^d = S^1 \times T^{d-1}$, so by \autoref{prop:cross-product-with-sphere-shifts-homology-by-dimension} we see that
    \begin{IEEEeqnarray*}{rCl}
        H_n(T^d) &\cong& H_n(T^{d-1}) \oplus H_{n-1}(T^{d-1})  \\
                 & \cong & \bigoplus_{i=0}^{d-1} \left( \bigoplus_{j=1}^{\binom{d-1}{i}} H_{n-i}(\star)\right) \oplus \bigoplus_{i=0}^{d-1} \left( \oplus_{j=1}^{\binom{d-1}{i}} H_{n-1-i}(\star) \right)  \\
                 & \cong &\bigoplus_{i=0}^{d-1} \left( \bigoplus_{j=1}^{\binom{d-1}{i}} H_{n-i}(\star)\right) \oplus \bigoplus_{i=1}^{d} \left( \bigoplus_{j=1}^{\binom{d-1}{i-1}} H_{n-i}(\star)\right)  \\
                 & \cong & 
    \end{IEEEeqnarray*}
    Now for $i=0$, we have  $\binom{d-1}{0} = \binom{d}{0} = 1$. and for  $i=d$ we get $\binom{d-1}{d-1} = 1 = \binom{d}{d}$. For  $1\leq i\leq d$, one uses the standard result
    \[
        \binom{d-1}{i} + \binom{d-1}{i-1} = \binom{d}{i}
    \] 
    to deduce the desired formula.

    The formula for the dimension axiom follows directly by plugging in $H_n(\star) = \mathbb{Z}\iff n = 0$.
\end{proof}

\begin{lemma}\label{lm:s-n-1-to-d-n-is-not-a-retract}
    For all $n\geq 1$, there is no map $f\colon D^n \to S^{n-1}$ such that $f(s) = s$ for all  $s\in S^{n-1}$. In other words, $S^{n-1}\to D^n$ is \textit{not} a retract.
\end{lemma}
\begin{proof}
   Assume such $f$ exists. Then for singular homology we have 
    \[
        \tilde{H}_{n-1}(S^{n-1}) \stackrel{i_*}{\longrightarrow} \tilde{H}_{n-1}(D^d) \stackrel{f_*}{\longrightarrow} \tilde{H}_{n-1}(S^{n-1})
   \] 
   plugging in the homologies we see that this is
   \[
   \mathbb{Z} \to 0 \to \mathbb{Z}
   \] 
   but there is no map $\mathbb{Z}\to \mathbb{Z}$ making this diagram commute.
\end{proof}

\begin{theorem}[Brouwer fixed point theorem]\label{thm:brouwer-fixed-point-theorem}
    Every map $f\colon D^n \to D^n$ has a fixed point, i.e. there exists $x\in D^n$ with $f(x) = x$.
\end{theorem}
\begin{proof}
    Assume $f(x)\neq x$  for all $x\in X$. Let $r_x$ be the ray starting  in  $f(x)$ through $x$.

    \missingfigure{Ray in $D^n$}

    Define $g(x)\coloneqq (r_x \cap S^{n-1}) \setminus \left \{f(x)\right\} $, i.e. intersect this ray with $S^{n-1}$, ignoring the beginning of the ray (if it intersects the sphere at all).
    This defines a continuous map $g\colon D^n \to S^{n-1}$.
    Observe that $g(s) = s$ for all  $s\in S^{n-1}$. But now, this contradicts  \autoref{lm:s-n-1-to-d-n-is-not-a-retract}, \contra. Hence, our assumption was wrong and every map $D^n \to D^n$ indeed has a fixed point.
\end{proof}
