%! TEX root = ../../master.tex
\lecture[]{So 07 Nov 2021}{Untitled}

We quickly describe the homotopy of the proof of \autoref{thm:combing-the-hedgehog-s-d-has-nowhere-vanishing-vector-field-iff-d-is-odd}. It is given by
\[
    (x,t) \mapsto \cos (\pi t) \cdot x + \sin(\pi t) \cdot \frac{v(x)}{\norm{v(x)}}
\]
One checks that for $t=0,1$ one gets  $\id, -\id$ respectively. Since  $\left< x,v(x) \right> =1$, the norm is also always one (and one actually only needs that $x,v(x)$ are linearly independent).

\begin{theorem}\label{thm:subsets-of-r-n-determine-dimension-of-space}
    Let $U\subset \mathbb{R}^m, V\subset \mathbb{R}^n$ be open and non-empty. If $f\colon U\cong V$, then  $n=m$.
\end{theorem}

\begin{proof}
    Pick some $x\in U$, let $H$ be the singular homology (or any other homology theory satisfying the dimension axiom).
    As the case $m=0$ is clear, for  $m>1$ we get
\[
    \begin{tikzcd}
        H_n(U, U\setminus \left\{x\right\} ) \ar{r}{f}[swap]{\cong} \ar[phantom]{d}[rotate=-90]{\cong}&  H_n(V, V\setminus f(x)) \ar[phantom]{d}[rotate=-90]{\cong}\\
        \ar[phantom]{d}[rotate=-90]{\cong}      H_n(\mathbb{R}^m, \mathbb{R}^m \setminus \left\{x\right\} ) & H_n(\mathbb{R}^n, \mathbb{R}^n \setminus f(x)) \ar[phantom]{d}[rotate=-90]{\cong}\\\
 \ar[phantom]{d}[rotate=-90]{\cong}       \tilde{H}_{n-1}(\mathbb{R}^m \setminus \left\{x\right\} ) & \tilde{H}_n(\mathbb{R}^n, \mathbb{R}^n \setminus f(x)) \ar[phantom]{d}[rotate=-90]{\cong}\\
        \mathbb{Z}\text{ iff n=m} & \mathbb{Z}
    \end{tikzcd}
\]
Note that we applied excision here to deduce
\[
    H_n(U, U\setminus \left\{\star\right\} ) \stackrel{\cong}{\longrightarrow} H_n(\mathbb{R}^m, \mathbb{R}^m \setminus \left\{\star\right\} )
\] 
using the subsets
\[
\underbrace{\mathbb{R}^m \setminus U}_{\text{closed}} \subset \underbrace{\mathbb{R}^m \setminus \left\{\star\right\}}_{\text{open}} \subset \mathbb{R}^m
\] 
as $\mathbb{R}^m \setminus (\mathbb{R}^m \setminus U) =U$.
\end{proof}

\begin{recap}
    \textit{Different meanings of homology:}
    \begin{itemize}
        \item The homology of a chain complex (see \autoref{def:homology-group}): Take $\faktor{\ker d_n}{\im d_{n+1}}$.
        \item A \nameref{def:homology-theory}: A family of functors $\Top^2 \to \Ab$ with certain properties.
        \item \nameref{def:singular-homlogy}: A special case of a homology theory.
    \end{itemize}
\end{recap}

\begin{dremark}
    One can also consider the category $\GrAb$ of  \vocab{graded abelian groups}, and then view a homology theory as a functor $\Top^2\to \GrAb$. For this, consider
    \[
    \bigoplus_{n\in \mathbb{Z}}H_n
    \] 
\end{dremark}

\section{Singular homology}
\subsection{Homotopy invariance}

\begin{definition}[Chain homotopy]\label{def:chain-homotopy}
    Let $f_\chainbullet, g_\chainbullet\colon  C_\chainbullet\to D_\chainbullet$ be chain maps. A \vocab{chain homotopy} $h_\chainbullet\colon f_\chainbullet\simeq g_\chainbullet$ is a family of homomorphism $h_n\colon C_n \to D_{n+1}$ such that
    \[
    d_{n+1}^D \circ  h_n + h_{n-1}d_n^C = f_n -g_n
    \] 
    i.e.
    \[
        \begin{tikzcd}[column sep = large]
        \ar[swap]{d}{d_{n+1}^C}       C_{n+1} \ar{r}{f_{n+1}-g_{n+1}} &  D_{n+1} \ar{d}{d_{n+1}^D}\\
        \ar[swap]{d}{d_{n}^C} \ar{ur}{h_n}       C_n \ar{r}{f_n-g_n} &  D_n \ar{d}{d_n^D}\\
        \ar{ur}{h_{n-1}}        C_{n-1} \ar[swap]{r}{f_{n-1}-g_{n-1}} &  D_{n-1}
    \end{tikzcd}
\]
\todo{finish commutative diagram}
\end{definition}

\begin{lemma}\label{lm:homotopic-chain-maps-induce-same-homology-maps}
    Let $f_\chainbullet, g_\chainbullet\colon C_\chainbullet\to D_\chainbullet$ be homotopic. Then $H_n(f_\chainbullet) = H_n(g_\chainbullet)$ for all  $n\in \mathbb{N}$.
\end{lemma}

\begin{proof}
    Let $x\in \ker d_n^C$ represent some element $[x] \in H_n(C_\chainbullet)$. Then
    \begin{IEEEeqnarray*}{rCl}
        H_n(f_\chainbullet)([x]) - H_n(g_\chainbullet) ([x]) &=& [f_n(x)] - [g_n(x)]  \\
                                                             & = & [f_n(x) - g_n(x)] \\
                                                             & = & [\underbrace{(d_{n+1}^D \circ  h_n)(x) }_{\in \im d_{n+1}^D}+ \underbrace{h_{n-1}(d_n^C(x)}_{=0}] \\
                                                             & = & 0
    \end{IEEEeqnarray*}
\end{proof}

The homotopy invariance of $H^{\sing}$ now follows from the next proposition:

\begin{proposition}\label{prop:homotopic-maps-of-topological-spaces-induce-homotopic-chain-maps}
    Let $f,g\colon X\to Y$ be homotopic. Then
    \[
        f_\chainbullet, g_\chainbullet\colon  C_\chainbullet^{\sing}(X) \to C_\chainbullet^{\sing}(Y)
    \]
    are homotopic.
\end{proposition}

\begin{lemma}\label{lm:composition-of-chain-homotopies}
    Let $h_\chainbullet$ be a chain homotopy between $f_\chainbullet, f_\chainbullet'\colon C_\chainbullet \to D_\chainbullet $ and let $g_\chainbullet \colon D_\chainbullet \to E_\chainbullet $ be a chain map.
    Then $(g_\chainbullet \circ h_\chainbullet $ is a chain homotopy between $g_\chainbullet \circ f_\chainbullet $ and $g_\chainbullet \circ f_\chainbullet '$.
\end{lemma}

\begin{proof}
    We compute
    \begin{IEEEeqnarray*}{rCl}
        d_{n+1}^E(g_{n+1} \circ h_n) + (g_n \circ h_{n-1}) \circ d_n^C & = & g_n \circ (d_{n+1}^D \circ h_n) + g_n \circ (h_{n-1} \circ d_n^C) \\
                                                                       & = & g_n \circ (d_{n+1}^D \circ h_n + h_{n-1} \circ d_n^C) \\
                                                                       & = & g_n \circ (f_n - f_n') \\
                                                                       & = & g_n \circ f_n - g_n \circ f_n'
    \end{IEEEeqnarray*}
\end{proof}

\begin{lemma}
    Let $i_0^X, i_1^X$ be the inclusions of
        \begin{equation*}
        \begin{array}{c c l} 
        X & \longrightarrow & X\times I \\
        x & \longmapsto &  (x,i)
        \end{array}
    \end{equation*}
    for $i=0,1$. If  $\left( i_0^X \right) _*$ and $\left( i_1^X \right) _*$ are homotopic, so are $f_\chainbullet , g_\chainbullet $ in \autoref{prop:homotopic-maps-of-topological-spaces-induce-homotopic-chain-maps}.
\end{lemma}

\begin{proof}
    Let $H$ be a homotopy from  $f$ to  $g$. Consider the compositions:
\[
     \begin{tikzcd}
         X \ar{r}{i_0} \ar[swap, bend right=30]{rr}{f}&  X\times I \ar{r}{H} &  Y
    \end{tikzcd}
    \begin{tikzcd}
        X \ar{r}{i_1} \ar[swap, bend right=30]{rr}{g}&  X\times I\ar{r}{H} &  Y
    \end{tikzcd}
\]
Then $f_* = H_* \circ  (i_0)_*$ and $g_* = H_* \circ (i_1)_*$. Let $h_*$ be a homotopy between $(i_0)_*$ and $(i_1)_*$. Then $H_* \circ h_*$ is a homotopy between $f_*$ and  $g_*$ by  \autoref{lm:composition-of-chain-homotopies}.
\end{proof}

We now sketch the idea of the proof of \autoref{prop:homotopic-maps-of-topological-spaces-induce-homotopic-chain-maps}.
Given a map $\Delta^n \stackrel{α}{\longrightarrow} X$, we can consider the map $\Delta^n\times I\stackrel{α\times \id}{\longrightarrow} X\times I$. Note that $\Delta^n \times I$ is $(n+1)$-dimensional, it is just not  $\Delta^{n+1}$.

We want to write $\Delta^n \times I$ as a union of $(n+1)$-simplizes in order to get an element in  $C_{n+1}^{\sing}(X\times I)$.
For this, we have to divide $\Delta^n\times I$.

\missingfigure{Cutting $\Delta^1 \times I$ }
\missingfigure{Cutting $\Delta^2 \times I$ }

In general, we consider the $(n+1)$-simplizes spanned by
 \[
     [(0,0), \ldots, (i,0), (i,1), \ldots,(n,1))]
\]
Also, we want to add these in a way such that the inner boundaries cancel. Then the rough idea is that we now have
\[
    \underbrace{\partial(\Delta^n\times I)}_{h_n(α)} = \underbrace{\partial\Delta^n \times I}_{h_{n-1}\partialα} + \underbrace{\Delta^n \times \left\{0,1\right\}}_{i_0-i_1} 
\]
(of course, this is not accurate at all, since $\partial$ is not even defined on $\Delta^n\times I$)

Now define $a_i^n\colon \Delta^{n+1} \to \Delta^n\times I$ as the affine linear map given by
    \begin{equation*}
    a_i^n: \left| \begin{array}{c c l} 
    \Delta^{n+1} & \longrightarrow & \Delta^n\times I \\
    j & \longmapsto &  \begin{cases}
        (j,0) & \text{if } 0\leq j\leq i \\
        (j-1,1) & \text{if } i<j\leq n+1
    \end{cases}
    \end{array} \right.
\end{equation*}
where $j$ is a vertex of $\Delta^{n+1}$.

\begin{oral}
    Note that $\Delta^n\times I$ is convex, and thus we can extend $a_i^n$ linearly, as we know the  images of the vertices.
\end{oral}

Furthermore, denote
 \[
     x_n \coloneqq \sum_{i=0}^n (-1)^i a_i^n \in C_{n+1}^{\sing}(\Delta^n \times I)
\] 

\begin{lemma}\label{lm:swapping-of-a-i-n-and-delta-j}
    If $j<i$, then
     \[
         a_i^n \circ δ^j = (δ^j \times   \id) \circ a_{i-1}^{n-1}
    \] 
    i.e. the square
    \[
    \begin{tikzcd}
        \Delta^{n+1}  \ar{r}{a_i^n} & \Delta^n \times I  \\
        \ar{u}{δ_j}   \Delta^n \ar[swap]{r}{a_{i-1}^{n-1}} & \Delta^{n-1}\times I\ar[swap]{u}{δ^j\times \id}
    \end{tikzcd}
    \]
    and similarly for $j>i$, we have
    \[
        a_i^n \circ δ^j = (δ^{j-1}\times \id) \circ a_i^{n-1}
    \] 
\end{lemma}

\begin{proof}
    Let $j<i$. Then
     \[
         k \stackrel{δ^j}{\longmapsto} \begin{cases}
             k & k < j \\
             k+ 1 & k \geq j
         \end{cases}
         \stackrel{a_i^n}{\longmapsto} \begin{cases}
             (k,0) & k < j \\
             (k+1,0) & j\leq k\leq i-1 \\
             (k,1) & i \leq k
         \end{cases}
    \]
    but we also have
    \[
    k \mapsto \begin{cases}
        (k,0) & k \leq  i- 1 \\
        (k-1,1) & i \leq k
    \end{cases}
    \longmapsto \begin{cases}
        (k,0) & k < j \\
        (k+1,0) & j \leq k\leq i-1 \\
        (k,1) & i \leq k
    \end{cases}
    \]
    so that we see that in both cases, the resulting image is
    \[
        ((0,0),\ldots,\hat{(j,0)},\ldots,(i,0),(i,1),\ldots,(n,1))
    \] 

    For $j>i$, we have
     \[
         k \stackrel{δ^j}{\longmapsto} \begin{cases}
        k & k < j \\
        k+1 & k \geq j
    \end{cases}
    \stackrel{a_i^n}{\longmapsto} \begin{cases}
        (k,0) & k \leq i \\
        (k-1,1) & i < k < j \\
        (k,1) & j \leq  k
    \end{cases}
    \] 
    as well as
    \[
    k \mapsto \begin{cases}
         (k,0) & k \leq  i \\
         (k-1,1) & k > i
    \end{cases}
    \longmapsto \begin{cases}
        (k,0) & k \leq i \\
        (k-1,1) & i < k < j \\
        (k,1) & j \leq  k
    \end{cases}
    \] 
    i.e. the resulting image is
    \[
        ((0,0),\ldots,(i,0),(i,1),\ldots,\hat{(j-1,1)},\ldots,(n,1))
    \] 
\end{proof}


\begin{dlemma}\label{lm:inner-boundaries-of-partition-of-delta-n-x-i-cancel}
   We have that $a_i^n \circ δ^i = a_{i-1}^n \circ δ^i$
\end{dlemma}

\begin{editor}
    We gave the next corollary the same number. to fix enumeration, this lemma is sub-numbered.
\end{editor}

\begin{proof}
    \[
    k \stackrel{δ^i}{\longmapsto} \begin{cases}
        k & k < i \\
        k+1 & k \geq i
    \end{cases}
    \stackrel{\longmapsto}{\longmapsto}  \begin{cases}
        (k,0) & k < i \\
        (k,1) & k \geq  i
    \end{cases}
    \]
    so that the resulting image is
    \[
        ((0,0),\ldots,(i-1,0),(i,1),\ldots,(n,1))
    \] 
\end{proof}

\begin{corollary}\label{cor:i-0-i-1-as-boundaries}
    \[
        \sum_{i=0}^n (a_i^n \circ δ^{i+1}) - (a_i^n \circ δ^i) = i_0 - i_1
    \] 
\end{corollary}

\begin{proof}
    We have
    \begin{IEEEeqnarray*}{rCl}
&&        \sum_{i=0}^n (a_j^n \circ δ^{i+1}) - (a_0^n \circ δ^i) \\
& = &  \sum_{i=1}^{n+1} (a_{i-1}^n \circ δ^i) - \sum_{i=0}^n (a_i^n \circ δ^i) \\
& \stackrel{\autoref{lm:swapping-of-a-i-n-and-delta-j}}{=}  & \underbrace{a_n^n \circ δ^{n+1}}_{k \mapsto (k,0)} - \underbrace{a_0^n \circ δ^0}_{k \mapsto (k,1)} \\
& = & i_0 - i_1
    \end{IEEEeqnarray*}
\end{proof}

\begin{lemma}
We have
\[
    \partial x_n = \sum_{j=0}^n (-1)^{j+1}(δ^j \times \id)_* (x_{n-1}) -i_0 + i_1
\] 
\end{lemma}

\begin{proof}
    \begin{IEEEeqnarray*}{rCl}
&&        \sum_{j=0}^n (-1)^j (δ^j \times \id)_* (x_{n-1}) \\
& = & \sum_{j=0}^n (-1)^j \sum_{i=0}^{n-1} (-1)^i (δ^j\times \id)_* (a_i^{n-1}) \\
& = & \sum_{i=0}^{n-1}\sum_{j=0}^n (-1)^{i+j} (δ^j \times \id)_* (a_i^{n-1}) \\
& = & \sum_{i=0}^{n-1}\sum_{j=0}^i (-1)^{i+j}(δ^j\times \id)_* (a_i^{n-1}) + \sum_{i=0}^{n-1} \sum_{j=i+1}^n (-1)^{i+j}(δ^j\times \id)_* (a_i^{n-1}) \\
& = & \sum_{i=1}^n \sum_{j=0}^{i-1} (-1)^{i+j+1}(δ^j \times \id)_*(a_{i-1}^{n-1}) + \sum_{i=0}^{n-1}\sum_{j=i+2}^{n+1} (-1)^{i+j+1}(δ^{j-1}\times \id)_*(a_i^{n-1})\\
& \stackrel{\autoref{lm:swapping-of-a-i-n-and-delta-j}}{=}  & \sum_{i=1}^n \sum_{j=0}^{i-1} (-1)^{i+j+1}(a_i^\cap \circ δ^j) + \sum_{i=0}^{n-1}\sum_{j=i+2}^{n+1} (-1)^{i+j+1}(a_i^n \circ δ^j) \\
& = & \sum_{i=0}^n \sum_{\substack{j=0 \\j\neq i,i+1}}^{n+1} (-1)^{i+j+1}(a_i^n \circ δ^j) \\
& = & -\partial x_n - \sum_{i=0}^n (-1)^{2i+1}(a_i^n \circ δ^i) - \sum_{i=0}^n (-1)^{2i+2}(a_i^n \circ δ^{i+1}) \\
& \stackrel{\autoref{cor:i-0-i-1-as-boundaries}}{=}  & -\partial x_n - i_0 + i_1
    \end{IEEEeqnarray*}
    so the proof is done.
\end{proof}

\begin{refproof}{prop:homotopic-maps-of-topological-spaces-induce-homotopic-chain-maps}
    Let $i_0 ^{x}, i_1^{x}\colon X \to  X\times I$ be the inclusions.
    We define a map
        \begin{equation*}
        h_n: \left| \begin{array}{c c l} 
            C_n^{\sing}(X) & \longrightarrow & C_{n+1}^{\sing}(X\times I) \\
            α & \longmapsto &  (α\times \id)_*(x_n)
        \end{array} \right.
    \end{equation*}
   Then
   \begin{IEEEeqnarray*}{rCl}
       \partial h_n(α) + h_{n-1}(\partialα) & = & (α\times \id)_* (\partial x_n) + \sum_{j=0}^n (-1)^j (α\circ δ^j\times \id)_*(x_{n-1}) \\
                                            & = & (α\times \id_*) \left[ \partial_n + \sum_{j=0}^n (-1)^j (δ^j \times \id)_* (x_{n-1}) \right] \\
                                            & = & (α\times \id)_*[-i_0+i_1] \\
                                            & = & (i_0^X)_*(α) + (i_1^X)_*(α)
   \end{IEEEeqnarray*}
   so this establishes a chain homotopy.
\end{refproof}

\begin{oral}
    In \todo{ref}, there is a possibly more elegant proof that uses less combinatorics. However, it makes heavily use of abstract nonsense, so that the resulting construction is not as concrete as ours.
\end{oral}

\missingfigure{Illustration of our chain homotopy}
